var str = `<p>
                  <strong>1. Introduction</strong>
                </p>
                <p>
                  Sentiment analysis is classifying whether the expressed
                  opinion in a sentence is positive, negative, or neutral.
                </p>
                <p>
                  <strong>2. Examples</strong>
                </p>
                <Table celled>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>Utterance</Table.HeaderCell>
                      <Table.HeaderCell>Label</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>
                        Nottingham forest have the nicest ground and setting in
                        the championship.
                      </Table.Cell>
                      <Table.Cell rowSpan="2" positive>
                        positive
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>
                        You know what that means - tap in and swipe up to make
                        these delicious shrampies fly!
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>
                        I went to school in a Maori dominated school in New
                        Zealand
                      </Table.Cell>
                      <Table.Cell rowSpan="2">neutral</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>
                        Rearmed and re-boilered by the Japanese, Suwo was
                        classified by the Imperial Japanese Navy as a coastal
                        defense ship in 1908
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>
                        Words cannot express the sadness I feel tonight.
                      </Table.Cell>
                      <Table.Cell rowSpan="2" negative>
                        negative
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>
                        Kenya's Patel dam bursts, sweeping away homes in Solai.
                      </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>

                <p>
                  <strong>3. Steps</strong>
                </p>
                <p>
                  Please modify the given sentence or try new sentence, until
                  you think the label given by the machine is incorrect.
                </p>

                <p>
                  You won't be payed for duplicate input and non-English input.
                </p>
                <strong>
                  If your sentences pass the validation, you will get 5x bonus
                  for each sentence. If your performance is bad, you won't be
                  able to participate the game.
                </strong>`;

var obj = {
  numberOfLimited: 1000,
  numberOfRequired: 1000,
  numberOfReceived: 20,
  task_id: 110010,
  typeOfTask: "sentiment_api",
  instruction: str
};

<p><strong>1. Introduction</strong></p>\r<p>Sentiment analysis is classifying whether the expressed opinion in a sentence is positive, negative, or neutral.</p>\r<p><strong>2. Examples</strong></p>\r
<table id="t1">\r  
<tr>\r    
  <th id="th1">Utterance</th>\r    
  <th id="th1">Label</th>\r  
  </tr>\r  <tr>\r    <td id="td1">Nottingham forest have the nicest ground and setting in the championship.</td>\r    
<td id="td1" rowspan="2" style={{ background: \'#FEF6F6\', color: \'#97443F\'}}>positive</td>\r  </tr>\r  <tr>\r    <td id="td1">You know what that means - tap in and swipe up to make these delicious shrampies fly!</td>\r  </tr>\r  <tr>\r    <td id="td1">I went to school in a Maori dominated school in New Zealand</td>\r    <td id="td1" rowspan="2">neutral</td>\r  </tr>\r  <tr>\r    <td id="td1">Rearmed and re-boilered by the Japanese, Suwo was classified by the Imperial Japanese Navy as a coastal defense ship in 1908</td>\r  </tr>\r  <tr>\r    <td id="td1">Words cannot express the sadness I feel tonight.</td>\r    <td id="td1" rowspan="2" style={{ background: \'#FDFFF6\', color: \'#3E6637\'}} >negative</td>\r  </tr>\r  <tr>\r    <td id="td1">Kenya\'s Patel dam bursts, sweeping away homes in Solai.</td>\r  </tr>\r</table>\r<p></p>\r<p><strong>3. Steps</strong></p>\r<p>Please modify the given sentence or try new sentence, until you think the label given by the machine is incorrect.</p>\r<p>You won\'t be payed for duplicate input and non-English input.</p>\r<strong>If your sentences pass the validation, you will get 5x bonus for each sentence. If your performance is bad, you won\'t be able to participate the game.</strong>

db.tasks.insert(obj);
