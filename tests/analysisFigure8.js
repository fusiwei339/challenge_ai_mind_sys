// var algoArr = db.algo.find().toArray();
// var algoObj = algoArr.reduce((acc, cur) => {
//   acc[cur.text_id] = cur;
//   return acc;
// }, {});
// // print(Object.keys(algoObj));

// db.validated.find().forEach(e => {
//   var algoE = algoObj[e.judgment_id];
//   if (algoE) {
//     e.lime_explanation = algoE.lime_explanation;
//     db.validated.save(e);
//   }
// });

db.getCollection("validated_new")
  .find({})
  .forEach(d => {
    var lime_explanation = d.lime_explanation;
    var arr = lime_explanation
      .sort((a, b) => Math.abs(b[1]) - Math.abs(a[1]))
      .filter(e => Math.abs(e[1]) > 0.2)
      .map(e => e[0]);
    d.desc = arr.length
      ? `"${arr.join(", ")}" affect the sentiment.`
      : `No specific word, but the whole sentence feels like ${
          d["AI result"]
        }.`;
    db.validated_new.save(d);
  });
