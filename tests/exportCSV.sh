dbname=ml
host=localhost:3001
coll=validated_new

keys=`mongo $dbname --host $host --eval "function z(c,e){var a=[];var d=Object.keys(c);for(var f in d){var b=d[f];if(typeof c[b]==='object'){var g=[],h=z(c[b],e+'.'+b);a=g.concat(a,h);}else a.push(e+'.'+b);}return a;}var a=[],b=db.$coll.findOne({}),c=Object.keys(b);for(var i in c){var j=c[i];if(typeof b[j]==='object'&&j!='_id'){var t1=[],t2=z(b[j],j);a=t1.concat(a,t2);}else a.push(j);}a.join(',');" --quiet`
# now use mongoexport with the set of keys to export the collection to csv
mongoexport --host $host -d $dbname -c ${coll} --fields "$keys" --type=csv --out ${coll}.csv;
