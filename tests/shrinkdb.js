var colls = [
  "alarz",
  "almr",
  "alms",
  "alrot",
  "imdb",
  "mr",
  "semeval",
  "sst",
  "yelp"
];
for (var i = 0; i < colls.length; i++) {
  var coll = colls[i];
  var arr = [];
  db[coll].find().forEach(function(d) {
    var temp = {
      text_id: d.text_id,
      text: d.text,
      data_source: d.data_source,
      positive_scores: d.positive_scores,
      negative_scores: d.negative_scores,
      neutral_scores: d.neutral_scores,
      human_label: d.human_label,
      prediction: d.prediction
    };
    arr.push(temp);
  });
  db[coll + "small"].drop();
  db[coll + "small"].insert(arr);
}
