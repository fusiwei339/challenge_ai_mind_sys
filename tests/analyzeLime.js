// // Speed up calls to hasOwnProperty
// var hasOwnProperty = Object.prototype.hasOwnProperty;

// function isEmpty(obj) {
//   if (obj == null) return true;
//   if (obj.length > 0) return false;
//   if (obj.length === 0) return true;

//   if (typeof obj !== "object") return true;

//   for (var key in obj) {
//     if (hasOwnProperty.call(obj, key)) return false;
//   }

//   return true;
// }

// function splitSentence(str) {
//   var WORD_BOUNDARY_CHARS = '\t\r\n\u00A0 !"#$%&()*+,-.\\/:;<=>?@[\\]^_`{|}~';
//   var SPLIT_REGEX = new RegExp("([^" + WORD_BOUNDARY_CHARS + "]+)");
//   return str.split(SPLIT_REGEX);
// }

// var arr = db.validated_new.find().toArray();

// var ret = [];
// for (var i = 0; i < arr.length; i++) {
//   var doc = arr[i];
//   var textArr = splitSentence(doc.utterance);
//   var textObj = doc.lime_explanation
//     .filter(e => Math.abs(e[1]) > 0.1)
//     .reduce((acc, cur) => {
//       acc[cur[0]] = cur[1];
//       return acc;
//     }, {});

//   if (isEmpty(textObj)) {
//     doc.words = [];
//     db.validated_new.save(doc);
//     continue;
//   }
//   var j = 0;
//   var words = [];
//   var tempWord = [];
//   //   print(JSON.stringify(textObj));
//   while (j < textArr.length) {
//     var word = textArr[j];
//     var val = textObj[textArr[j]];
//     if (val) {
//       tempWord.push({
//         word,
//         val
//       });
//     } else {
//       if (tempWord.length) {
//         words.push(
//           tempWord.reduce((acc, cur, idx) => {
//             acc.word = cur.word + " ";
//             acc.val = cur.val;
//             acc.len = idx;
//             return acc;
//           }, {})
//         );
//         tempWord = [];
//       }
//     }
//     if (j === textArr.length - 1 && tempWord.length) {
//       words.push(
//         tempWord.reduce((acc, cur, idx) => {
//           acc.word = cur.word + " ";
//           acc.val = cur.val;
//           acc.len = idx;
//           return acc;
//         }, {})
//       );
//     }
//     j++;
//   }
//   doc.words = words;
// }
// db.test.drop();
// db.test.insertMany(arr);

var sentiment = "neutral";
var arr = db.test.find({ "valided result": sentiment }).toArray();
var ret = [];
var obj = {};
for (var i = 0; i < arr.length; i++) {
  var words = arr[i].lime_explanation;
  for (var j = 0; j < words.length; j++) {
    var word = words[j];
    if (Math.abs(word[1]) > 0.1) {
      if (obj[word[0]]) {
        if (word[0] === "watch") continue;
        obj[word[0]].val.push(word[1]);
        obj[word[0]].times++;
      } else {
        obj[word[0]] = {};
        obj[word[0]].val = [word[1]];
        obj[word[0]].word = word[0];
        obj[word[0]].times = 1;
        obj[word[0]].sentiment = sentiment;
      }
    }
  }
}

// db.stat.drop();
db.stat.insertMany(
  Object.keys(obj).map(function(key) {
    return obj[key];
  })
);
