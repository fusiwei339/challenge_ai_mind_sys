import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

/** Create a Meteor collection. */
const TaskConfigs = new Mongo.Collection('taskconfigs');

/** Create a schema to constrain the structure of documents associated with this collection. */
const TaskConfigSchema = new SimpleSchema({
  task_id: Number,
  target_id: { // task id to validate
    type: Number,
    optional: true
  },
  judgment_id: { // judgment id to validate
    type: String,
    optional: true
  },
  utterance: String,
  result: {
    type: String,
    optional: true
  },
  label: {
    type: String,
    optional: true
  },
  external_job_id: {
    type: String,
    optional: true
  },
  owner: {
    type: String,
    optional: true
  },
  typeOfService: {
    type: String,
    optional: true,
  },
  numberOfRequired: Number,
  numberOfReceived: Number,
  validated: {
    type:Boolean,
    optional: true
  }
}, { tracker: Tracker });

/** Attach this schema to the collection. */
TaskConfigs.attachSchema(TaskConfigSchema);

/** Make the collection and schema available to other code. */
export { TaskConfigs, TaskConfigSchema };
