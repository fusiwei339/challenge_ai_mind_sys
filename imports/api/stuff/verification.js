import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

/** Create a Meteor collection. */
const Verifications = new Mongo.Collection('verifications');

/** Create a schema to constrain the structure of documents associated with this collection. */
const VerificationSchema = new SimpleSchema({
  code: String,
  task_id: Number,
  external_job_id: {
    type: Number,
    optional: true
  },
  numOfAllJudgments: Number,
  owner: String,
  createdAt: Number,
  resultJudgments: [String],
  validated: Boolean
}, { tracker: Tracker });

/** Attach this schema to the collection. */
Verifications.attachSchema(VerificationSchema);

/** Make the collection and schema available to other code. */
export { Verifications, VerificationSchema };
