import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

/** Create a Meteor collection. */
const Judgments = new Mongo.Collection('judgments');

/** Create a schema to constrain the structure of documents associated with this collection. */
const JudgmentSchema = new SimpleSchema({
  utterance: String,
  typeOfTask: String,
  typeOfJudgment: {
    type: String, // result or test
    optional: true,
  },
  typeOfService: {
    type: String,
    optional: true,
  },
  task_id: Number,
  taskconfig_id: {
    type: String,
    optional: true,
  },
  label: {
    type: String,
    optional: true,
  },
  result: String,
  level: {
    type: Number,
    optional: true,
  },
  reason: {
    type: String,
    optional: true,
  },
  owner: String,
  createdAt: Number,
  verified: Boolean
}, { tracker: Tracker });

/** Attach this schema to the collection. */
Judgments.attachSchema(JudgmentSchema);

/** Make the collection and schema available to other code. */
export { Judgments, JudgmentSchema };
