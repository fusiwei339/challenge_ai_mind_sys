import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

/** Create a Meteor collection. */
const Tasks = new Mongo.Collection('tasks');

/** Create a schema to constrain the structure of documents associated with this collection. */
const TaskSchema = new SimpleSchema({
  task_id: Number,
  utterance: {
    type: String,
    optional: true
  },
  // type: String,
  // language: String,
  numberOfLimited: {
    type: Number,
    optional: true
  },
  numberOfRequired: Number,
  numberOfReceived: Number,
  dataSource: {
    type: String,
    optional: true
  },
  typeOfTask: {
    type: String,
    optional: true,
  }
}, { tracker: Tracker });

/** Attach this schema to the collection. */
Tasks.attachSchema(TaskSchema);

/** Make the collection and schema available to other code. */
export { Tasks, TaskSchema };
