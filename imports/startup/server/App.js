import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import { Tasks } from '../collections/tasks.js';

import Task from './Task.js';
// import AccountsUIWrapper from './AccountsUIWrapper.js';

// App component - represents the whole app
export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hideCompleted: false,
    };
  }

  render() {
    return (
      <div className="container">
        <header>
          <h1>Annotation Tasks</h1>

          <label className="hide-completed">
            <input
              type="checkbox"
              readOnly
            />
            Hide Completed Tasks
          </label>

          { Meteor.userId() ?
            <div className="new-task" >
              <a href="/sentiment">Sentiment</a>
            </div> : 'Please log in to work on tasks.'
          }
        </header>

      </div>
    );
  }

  navigate(service) {
      return (
          <service />
      );
  }
}

// export default withTracker(() => {
//   Meteor.subscribe('tasks');
//
//   return {
//     tasks: Tasks.find({}, { sort: { createdAt: -1 } }).fetch(),
//     incompleteCount: Tasks.find({ checked: { $ne: true } }).count(),
//     currentUser: Meteor.user(),
//   };
// })(App);
