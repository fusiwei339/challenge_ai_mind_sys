import { Meteor } from "meteor/meteor";
import { Roles } from "meteor/alanning:roles";
import { Tasks } from "../../api/stuff/task.js";

/** Initialize the database with a default data document. */
// function addData(data) {
//   // console.log(`  Adding: ${data.name} (${data.owner})`);
//   Tasks.insert(data);
// }
//
// /** Initialize the collection if empty. */
// if (Tasks.find().count() === 0) {
//   if (Meteor.settings.defaultData) {
//     console.log('Creating default data.');
//     Meteor.settings.defaultData.map(data => addData(data));
//   }
// }

/** This subscription publishes only the documents associated with the logged in user */
Meteor.publish("Task", function publish(task_id) {
  if (this.userId) {
    // const username = Meteor.users.findOne(this.userId).username;
    return Tasks.find({ task_id: task_id }, { limit: 1 });
  }
  return this.ready();
});

Meteor.publish("OneTask", function publish(juds) {
  console.log(juds);

  if (this.userId) {
    // const username = Meteor.users.findOne(this.userId).username;
    return Tasks.findOne({ task_id: juds });
  }
  return this.ready();
});

/** This subscription publishes all documents regardless of user, but only if the logged in user is the Admin. */
// Meteor.publish('StuffAdmin', function publish() {
//   if (this.userId && Roles.userIsInRole(this.userId, 'admin')) {
//     return Stuffs.find();
//   }
//   return this.ready();
// });
