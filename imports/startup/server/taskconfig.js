import { Meteor } from "meteor/meteor";
import { Roles } from "meteor/alanning:roles";
import { TaskConfigs } from "../../api/stuff/taskconfig.js";

/** This subscription publishes only the documents associated with the logged in user */
Meteor.publish("TaskConfig", function publish(task_id) {
  if (this.userId) {
    // const username = Meteor.users.findOne(this.userId).username;
    return TaskConfigs.find(
      {
        task_id: task_id,
        $where: "this.numberOfRequired > this.numberOfReceived"
      },
      { limit: 10 }
    );
  }
  return this.ready();
});
