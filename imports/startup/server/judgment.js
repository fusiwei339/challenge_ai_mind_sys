import { Meteor } from "meteor/meteor";
import { Roles } from "meteor/alanning:roles";
import { Judgments } from "../../api/stuff/judgment.js";
import { HTTP } from "meteor/http";

/** Initialize the database with a default data document. */
// function addData(data) {
//   // console.log(`  Adding: ${data.utterance_id} (${data.owner})`);
//   Judgments.insert(data);
// }
//
// /** Initialize the collection if empty. */
// if (Judgments.find().count() === 0) {
//   if (Meteor.settings.defaultData) {
//     console.log('Creating default data.');
//     Meteor.settings.defaultData.map(data => addData(data));
//   }
// }

/** This subscription publishes only the documents associated with the logged in user */
Meteor.publish("Judgment", function publish(task_id) {
  if (this.userId) {
    const username = Meteor.users.findOne(this.userId).username;
    return Judgments.find({ owner: username, task_id: task_id }, { limit: 50 });
  }
  return this.ready();
});

Meteor.methods({
  // analyzeSentimentWatson: function(utterance, owner, createdAt, task_id) {
  //   try {
  //     const user = owner;
  //     console.log("watson");
  //     var NaturalLanguageUnderstandingV1 = require("watson-developer-cloud/natural-language-understanding/v1.js");

  //     api_key =
  //       "YzVmMTcwNTMtZDE5Yy00MWIyLWJiYjYtZjNkYzYwMGY1MzFkOjRDZWdBNkM2U04wQw==";
  //     api_url =
  //       "https://gateway-s.watsonplatform.net/natural-language-understanding/api/v1/analyze?version=2018-03-16";
  //     header = {
  //       Authorization: "Basic " + api_key,
  //       "Content-type": "application/json"
  //     };

  //     var natural_language_understanding = new NaturalLanguageUnderstandingV1({
  //       username: "adc5094c-e56f-4f8e-b749-9af4305009fe",
  //       password: "6LVGTNlySKyp",
  //       version: "2018-03-16",
  //       url:
  //         "https://gateway.watsonplatform.net/natural-language-understanding/api"
  //     });

  //     var parameters = {
  //       text: utterance,
  //       features: {
  //         sentiment: {}
  //       }
  //     };

  //     natural_language_understanding.analyze(
  //       parameters,
  //       Meteor.bindEnvironment(function(err, response) {
  //         process.on("unhandledRejection", r => console.log(r));

  //         if (err) {
  //           var res =
  //             '{"sentiment": { "document": { "score":0, "label":"non-sense input"}}}';
  //           res = JSON.parse(res);
  //           res = JSON.stringify(res, null, 2);
  //           Judgments.insert({
  //             utterance: utterance,
  //             typeOfTask: "sentiment_api",
  //             typeOfJudgment: "sentiment_api_test",
  //             task_id: task_id,
  //             typeOfService: "ibm_cloud",
  //             result: res,
  //             owner: user,
  //             createdAt: createdAt,
  //             verified: false
  //           });
  //         } else {
  //           var res = JSON.stringify(response, null, 2);
  //           Judgments.insert({
  //             utterance: utterance,
  //             typeOfTask: "sentiment_api",
  //             typeOfJudgment: "sentiment_api_test",
  //             task_id: task_id,
  //             typeOfService: "ibm_cloud",
  //             result: res,
  //             owner: user,
  //             createdAt: createdAt,
  //             verified: false
  //           });
  //         }
  //       })
  //     );
  //   } catch (e) {
  //     console.log(e);
  //   }
  // },

  analyzeSentimentWatson: function(utterance, owner, createdAt, task_id) {
    try {
      // process.on('unhandledRejection', r => console.log(r));

      const user = owner;
      console.log("watson");

      // api_key='fwef.'
      const header = {
        "Content-type": "application/json"
      };

      // const api_url="http://18.223.46.185:5000/lime"
      const api_url="http://127.0.0.1:5000/lime"

      var parameters = {
        text: utterance,
      };

      HTTP.call(
        "POST",
        api_url,
        {
          data: parameters,
          headers: header
        },
        (error, result) => {
          if (error) {
            // console.log(error["response"]["data"]["language"]);
            var res =
              '{"sentiment": { "document": { "score":0, "label":"system error"}}}';
            var typeOfJudgment = "sentiment_api_error";

            if (
              error &&
              error["response"] &&
              error["response"]["data"] &&
              (error["response"]["data"]["language"] == "nl" ||
                error["response"]["data"]["language"] == "unknown")
            ) {
              res =
                '{"sentiment": { "document": { "score":0, "label":"non-sense input"}}}';
              typeOfJudgment = "sentiment_api_test";
            }

            res = JSON.parse(res);
            res = JSON.stringify(res, null, 2);
            Judgments.insert({
              utterance: utterance,
              typeOfTask: "sentiment_api",
              typeOfJudgment: typeOfJudgment,
              typeOfService: "ibm_cloud",
              task_id: task_id,
              result: JSON.stringify(error),
              owner: user,
              createdAt: createdAt,
              verified: false
            });
          } else {
            var res = result["data"];
            // console.log(res)
            Judgments.insert({
              utterance: utterance,
              typeOfTask: "sentiment_api",
              typeOfJudgment: "sentiment_api_test",
              typeOfService: "ibm_cloud",
              task_id: task_id,
              result: JSON.stringify(res, null, 2),
              owner: user,
              createdAt: createdAt,
              verified: false
            });
          }
        }
      );
    } catch (e) {
      var res =
        '{"sentiment": { "document": { "score":0, "label":"system error"}}}';
      res = JSON.parse(res);
      res = JSON.stringify(res, null, 2);
      Judgments.insert({
        utterance: utterance,
        typeOfTask: "sentiment_api",
        typeOfJudgment: "sentiment_api_error",
        typeOfService: "ibm_cloud",
        task_id: task_id,
        result: JSON.stringify(e),
        // owner: user,
        createdAt: createdAt,
        verified: false
      });
      console.log(e)
    }
  },

  analyzeSentimentGoogle: function(utterance, owner, createdAt, task_id) {
    // process.on('unhandledRejection', r => console.log(r));

    const user = owner;
    // console.log("google");

    var env_dot = require("dotenv").config();
    console.log("dot " + env_dot);
    // console.log(process.env.GOOGLE_APPLICATION_CREDENTIALS);

    const language = require("@google-cloud/language");

    console.log(process.env);
    // Creates a client
    // const client = new language.LanguageServiceClient({
    // keyFilename: '/home/ubuntu/speech-collecting-cf583e9e66a6.json'
    // });

    const client = new language.LanguageServiceClient();

    // Prepares a document, representing the provided text
    const document = {
      // key: "AIzaSyDCcs-zckAo4aZo437u0wMquUag1S883pc",
      content: utterance,
      type: "PLAIN_TEXT"
    };

    // Detects the sentiment of the document
    client
      .analyzeSentiment({ document: document })
      .then(results => {
        var res = JSON.stringify(results[0].documentSentiment);
        // console.log(res);
        Judgments.insert({
          utterance: utterance,
          typeOfTask: "sentiment_api",
          typeOfJudgment: "sentiment_api_test",
          typeOfService: "google_cloud",
          task_id: task_id,
          result: res,
          owner: user,
          createdAt: createdAt,
          verified: false
        });
      })
      .catch(err => {
        // var res = '{ "score":0, "magnitude":0, "label":"system error"}';
        // res = JSON.parse(res);
        // var res = JSON.stringify(err);
        Judgments.insert({
          utterance: utterance,
          typeOfTask: "sentiment_api",
          typeOfJudgment: "sentiment_api_error",
          typeOfService: "google_cloud",
          task_id: task_id,
          result: err.toString(),
          owner: user,
          createdAt: createdAt,
          verified: false
        });
      });
  }
});
