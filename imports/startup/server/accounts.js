import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';

/* eslint-disable no-console */

function createUser(email, password, gender, language, age, role) {
  console.log(`  Creating user ${email}.`);

  const userID = Accounts.createUser({
    username: email,
    email: email,
    password: password,
    profile: {
        agender: gender,
        language: language,
        age: age,
        error: 0,
        goldScore: 0,
        silverScore: 0,
    }
  });
  if (role === 'admin') {
    Roles.addUsersToRoles(userID, 'admin');
  }
}

Meteor.methods({
  signinNP: function(worker_id) {
    // var user = Meteor.users.find({"username": worker_id}, {limit: 1});
    // console.log(user);
    var user = Meteor.users.findOne({"username": worker_id});

    if (user) {
      // console.log('exist');
      Meteor.users.update({"username": worker_id}, {$set: {"profile.goldScore": 0, "profile.silverScore": 0}});
      var stampedLoginToken = Accounts._generateStampedLoginToken();
      Accounts._insertLoginToken(user._id, stampedLoginToken);
      return stampedLoginToken;
    } else {
      return null;
    }
  }
});

/** When running app for first time, pass a settings file to set up a default user account. */
if (Meteor.users.find().count() === 0) {
  if (Meteor.settings.defaultAccounts) {
    console.log('Creating the default user(s)');
    Meteor.settings.defaultAccounts.map(({ email, password, role }) => createUser(email, password, role));
  } else {
    console.log('Cannot initialize the database!  Please invoke meteor with a settings file.');
  }
}
