import { Meteor } from "meteor/meteor";
import { Roles } from "meteor/alanning:roles";
import { Verifications } from "../../api/stuff/verification.js";

/** Initialize the database with a default data document. */
// function addData(data) {
//   // console.log(`  Adding: ${data.utterance_id} (${data.owner})`);
//   TestJudgments.insert(data);
// }
//
// /** Initialize the collection if empty. */
// if (TestJudgments.find().count() === 0) {
//   if (Meteor.settings.defaultData) {
//     console.log('Creating default data.');
//     Meteor.settings.defaultData.map(data => addData(data));
//   }
// }

/** This subscription publishes only the documents associated with the logged in user */

Meteor.publish("Verification", function publish(task_id) {
  if (this.userId) {
    const username = Meteor.users.findOne(this.userId).username;
    return Verifications.find({ owner: username, task_id: task_id });
  }
  return this.ready();
});
