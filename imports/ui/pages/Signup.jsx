import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Container, Form, Grid, Header, Message, Segment } from 'semantic-ui-react';
import { Accounts } from 'meteor/accounts-base';
import { Random } from 'meteor/random'

const dropoptions = [
  { key: 'e', text: 'English', value: 'english' },
  { key: 'g', text: 'German', value: 'german' },
  { key: 'i', text: 'Italian', value: 'italian' },
  { key: 'f', text: 'French', value: 'french' },
  { key: 'c', text: 'Chinese', value: 'chinese' },
]

export default class Signup extends React.Component {
  /** Initialize state fields. */
  constructor(props) {
    super(props);
    this.state = { email: '', password: '', gender: '', language:'', age:'', error: '', redirectToReferer: false };
    // Ensure that 'this' is bound to this component in these two functions.
    // https://medium.freecodecamp.org/react-binding-patterns-5-approaches-for-handling-this-92c651b5af56
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  /** Update the form controls each time the user interacts with them. */
  handleChange(e, { name, value }) {
    this.setState({ [name]: value });
  }

  /** Handle Signup submission using Meteor's account mechanism. */
  handleSubmit() {
    const { email, password, gender, language, age } = this.state;

    if (!gender || !language || !age) {
      this.setState({ error:
        !gender ? 'Please choose your gender' :
        !age ? 'Please input your age' : 'Please select your language' });
      return;
    }

    var nemail = email;
    var npassword = password;

    if (!email) {
      nemail = this.props.match.params.worker_id;
      npassword = "" + Random.id();
    }

    Accounts.createUser({
      email: nemail,
      username: nemail,
      password: npassword,
      profile: {
        gender: gender,
        language: language,
        age: age,
        error: 0,
        goldScore: 0,
        silverScore: 0,
      }}, (err) => {
        if (err) {
          this.setState({ error: err.reason });
        } else {
          this.setState({ error: '', redirectToReferer: true });
        }
    });
  }

  /** Display the signup form. */
  render() {
    var task_param = new URLSearchParams(this.props.location.search);
    var pathname = '/signin'
      + (this.props.match.params.worker_id ? '/' + this.props.match.params.worker_id : '')
      + (task_param ? '?' + task_param : '');
    const from = '/'
      + (task_param.get("task") ? task_param.get("task") : '')
      + (task_param.get("job") ? '?job=' + task_param.get("job") : '');

    if (this.state.redirectToReferer) {
      return <Redirect to={from}/>;
    }

    if (this.props.match.params.worker_id) {
      return (
        <Container>
          <Grid textAlign="center" verticalAlign="middle" centered columns={2}>
            <Grid.Column>
              <Header as="h2" textAlign="center">
                Fill in your information
              </Header>
              <Form onSubmit={this.handleSubmit}>
                <Segment stacked>
                  <Form.Group inline>
                    <label>Gender</label>
                    <Form.Radio name='gender' label='Female' value='female' checked={this.state.gender === 'female'} onChange={this.handleChange} />
                    <Form.Radio name='gender' label='Male' value='male' checked={this.state.gender === 'male'} onChange={this.handleChange} />
                  </Form.Group>
                  <Form.Input
                    label="Age"
                    name="age"
                    placeholder="Age"
                    type="number"
                    onChange={this.handleChange}
                  />
                  <Form.Select fluid name='language' label='Languege' options={dropoptions} placeholder='Languege' onChange={this.handleChange}/>
                  <Form.Button content="Submit"/>
                </Segment>
              </Form>
              {this.state.error === '' ? (
                  ''
              ) : (
                  <Message
                      error
                      header="Registration was not successful"
                      content={this.state.error}
                  />
              )}
            </Grid.Column>
          </Grid>
        </Container>
      );
    } else {
      return (
        <Container>
          <Grid textAlign="center" verticalAlign="middle" centered columns={2}>
            <Grid.Column>
              <Header as="h2" textAlign="center">
                Register your account
              </Header>
              <Form onSubmit={this.handleSubmit}>
                <Segment stacked>
                  <Form.Input
                      label="Email"
                      icon="user"
                      iconPosition="left"
                      name="email"
                      type="email"
                      placeholder="E-mail address"
                      onChange={this.handleChange}
                  />
                  <Form.Input
                      label="Password"
                      icon="lock"
                      iconPosition="left"
                      name="password"
                      placeholder="Password"
                      type="password"
                      onChange={this.handleChange}
                  />
                  <Form.Group inline>
                    <label>Gender</label>
                    <Form.Radio name='gender' label='Female' value='female' checked={this.state.gender === 'female'} onChange={this.handleChange} />
                    <Form.Radio name='gender' label='Male' value='male' checked={this.state.gender === 'male'} onChange={this.handleChange} />
                  </Form.Group>
                  <Form.Input
                    label="Age"
                    name="age"
                    placeholder="Age"
                    type="number"
                    onChange={this.handleChange}
                  />
                  <Form.Select
                    label="Languege"
                    fluid name='language'
                    options={dropoptions}
                    placeholder='Languege'
                    onChange={this.handleChange}/>
                  <Form.Button content="Submit"/>
                </Segment>
              </Form>
              <Message>
                Already have an account? Login <Link to={pathname}>here</Link>
              </Message>
              {this.state.error === '' ? (
                  ''
              ) : (
                  <Message
                      error
                      header="Registration was not successful"
                      content={this.state.error}
                  />
              )}
            </Grid.Column>
          </Grid>
        </Container>
      );
    }
  }
}
