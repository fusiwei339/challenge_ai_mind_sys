import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Input, Radio, Form, Grid, Segment, Container, Header, Loader, Button, Accordion, Message, Icon, Table } from 'semantic-ui-react';
import { withTracker } from 'meteor/react-meteor-data';
import { Tracker } from 'meteor/tracker';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import AutoForm from 'uniforms-semantic/AutoForm';
import TextField from 'uniforms-semantic/TextField';
import NumField from 'uniforms-semantic/NumField';
import SelectField from 'uniforms-semantic/SelectField';
import SubmitField from 'uniforms-semantic/SubmitField';
import HiddenField from 'uniforms-semantic/HiddenField';
import ErrorsField from 'uniforms-semantic/ErrorsField';
import { Bert } from 'meteor/themeteorchef:bert';

import { Tasks } from '/imports/api/stuff/task';
import { TaskConfigs } from '/imports/api/stuff/taskconfig';
import { Judgments, JudgmentSchema } from '/imports/api/stuff/judgment';
import { Verifications } from '/imports/api/stuff/verification';
import { SentimentItem } from '/imports/ui/components/SentimentItem';

/** Renders a table containing all of the Stuff documents. Use <StuffItem> to render each row. */
class Sentiment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      config: null,
      job_id: undefined,
      task_id: 0,
      instruction: 1,
      makesense: Array(5).fill(''),
      result: Array(5).fill(''),
      reason: Array(5).fill(''),
      createdAt: 0,
      state: 'init',
      error: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.submitDone = this.submitDone.bind(this);
    this.handleArrayChange = this.handleArrayChange.bind(this);
  }

  componentWillMount() {
    if (this.props.match.params.task_id) {
      this.setState({ task_id: Number(this.props.match.params.task_id) });
    }

    var task_param = new URLSearchParams(this.props.location.search);

    if (task_param) {
      this.setState({ job_id: Number(task_param.get("job")) });
    }
  }

  handleClick(e, titleProps) {
    this.setState({ instruction: !this.state.instruction });
  }

  handleChange(e, { name, value }) {
    this.setState({ [name]: value });
  }

  handleArrayChange(e, { name, value, ind }) {
    const sub = name.substring(0, name.length - 1);
    const narray = this.state[sub].slice();
    narray[ind] = value;
    this.setState({[sub]: narray});

    // console.log(this.state.makesense);
    // console.log(this.state.result);
  }

  render() {
    return (this.props.ready) ? this.renderPage() : <Loader>Getting data</Loader>;
  }

  renderSegment(configs, i) {
    if (configs[i] == undefined) {
      return (<div></div>);
    }

    return (
      <Segment>
        <p>
          <label>Sentence: {configs[i].utterance}</label>
        </p>
        <Form.Field>
          <label>Is the sentence in English and does it make sense?</label>
        </Form.Field>
        <Form.Group inline style={{ marginTop: '15px'}}>
          <Form.Radio label='Yes' name={'makesense'+i} value='yes' ind={i} checked={this.state.makesense[i] === 'yes'} onChange={this.handleArrayChange} width={2}/>
          <Form.Radio label='No' name={'makesense'+i} value='no' ind={i} checked={this.state.makesense[i] === 'no'} onChange={this.handleArrayChange} width={2}/>
        </Form.Group>
        { this.state.makesense[i] === 'yes' ?
          <Form.Field>
          <label>What is the sentiment of the sentence?</label>
          <Form.Group inline style={{ marginTop: '15px'}}>
            <Form.Radio label='Positive' name={'result'+i} value='positive' ind={i} checked={this.state.result[i] === "positive"} onChange={this.handleArrayChange} width={2} />
            <Form.Radio label='Neutral' name={'result'+i} value='neutral' ind={i} checked={this.state.result[i] === 'neutral'} onChange={this.handleArrayChange} width={2} />
            <Form.Radio label='Negative' name={'result'+i} value='negative' ind={i} checked={this.state.result[i] === 'negative'} onChange={this.handleArrayChange} width={2} />
          </Form.Group>
          <Form.Input name={'reason'+i} ind={i} label="Explain your answer (optional)" onChange={this.handleArrayChange}/>
          </Form.Field>
        : <Form.Field>
            <Form.Input name={'reason'+i} ind={i} label="Explain your answer (optional)" onChange={this.handleArrayChange}/>
          </Form.Field>
        }
      </Segment>
    );
  }

  /** Render the page once subscriptions have been received. */
  renderPage() {
    if (this.state.state == 'verify') {
      const tourl = '/verification?task=' + this.state.task_id;
      return <Redirect to={tourl} />;

    } else if (this.props.tasks.length == 0
        || this.props.tasks[0].numberOfReceived >= this.props.tasks[0].numberOfRequired
        || this.state.state == 'quit'
        || Meteor.user().profile.error > 3) {
      return (
        <Grid container centered>
            <div className="no_task">No more task :)</div>
        </Grid>);

    } else {
      var judgments = [];
      var configs = [];

      for (var i = 0; i < this.props.judgments.length; ++i) {
        judgments.push(this.props.judgments[i].taskconfig_id);
      }

      for (var i = 0; i < this.props.taskconfigs.length; ++i) {
        if (this.props.taskconfigs[i].numberOfRequired > this.props.taskconfigs[i].numberOfReceived
          && !judgments.includes(this.props.taskconfigs[i]._id)
          && this.props.taskconfigs[i].owner != Meteor.user().username) {
          configs.push(this.props.taskconfigs[i]);
          if (configs.length == 5) {
            break;
          }
        }
      }

      if (configs.length == 0) {
        return (
          <Grid container centered>
            <Grid.Row>
              <div className="no_task">No more task :)</div>
            </Grid.Row>
            <Grid.Row>
              {this.state.state == 'continue' ?
                <Form.Group className="no_task">
                  <Form.Button content="Get verification code" type='button' onClick={this.submitDone}/>
                </Form.Group>
                : null
              }
            </Grid.Row>
          </Grid>);
      } else {
        return (
          <Grid container centered>
            <Grid.Column>
              {/*************** instruction ***************/}
              <Accordion fluid styled style={{ marginBottom: '20px', marginTop: '20px'}}>
                <Accordion.Title active={this.state.instruction == 1} onClick={this.handleClick}>
                  <Icon name='dropdown' />
                  Instruction
                </Accordion.Title>
                <Accordion.Content active={this.state.instruction == 1}>
                  <p><strong>1. Introduction</strong></p>
                  <p>
                    Sentiment analysis is classifying the polarity of a given text at the document,
                    sentence, or feature/aspect level—whether the expressed opinion in a document,
                    a sentence or an entity feature/aspect is positive, negative, or neutral.
                  </p>
                  <p><strong>2. Examples</strong></p>
                  <Table celled>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Utterance</Table.HeaderCell>
                        <Table.HeaderCell>Label</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>Nottingham forest have the nicest ground and setting in the championship.</Table.Cell>
                        <Table.Cell rowSpan='2' positive>positive</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>You know what that means - tap in and swipe up to make these delicious shrampies fly!</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>I went to school in a Maori dominated school in New Zealand</Table.Cell>
                        <Table.Cell rowSpan='2'>neutral</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Rearmed and re-boilered by the Japanese, Suwo was classified by the Imperial Japanese Navy as a coastal defense ship in 1908</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Words cannot express the sadness I feel tonight.</Table.Cell>
                        <Table.Cell rowSpan='2' negative>negative</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Kenya's Patel dam bursts, sweeping away homes in Solai.</Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                  <p><strong>3. Steps</strong></p>
                  <p>
                    Please determine if the given sentiment of the sentence is correct.
                  </p>
                </Accordion.Content>
              </Accordion>

              {/*************** sentiment determination ***************/}
              <Container>
                <Form onSubmit={() => this.handleSubmit(configs)}>
                  {this.renderSegment(configs, 0)}
                  {this.renderSegment(configs, 1)}
                  {this.renderSegment(configs, 2)}
                  {this.renderSegment(configs, 3)}
                  {this.renderSegment(configs, 4)}

                  <Form.Group inline>
                    <Form.Button content="Submit"/>
                    <Form.Button content="Done" name='state' value='done' type='button' onClick={this.handleChange}/>
                  </Form.Group>

                  {this.state.state == 'done' ?
                    <div>
                      <Form.Field>
                      <label>You want to quit?</label>
                      </Form.Field>
                      <Form.Group inline>
                        <Form.Button content="Yes" type='button' onClick={this.submitDone}/>
                        <Form.Button content="I want to continue :)" name='state' value='continue' type='button' onClick={this.handleChange}/>
                      </Form.Group>
                    </div>
                    : null
                  }
                </Form>


                {this.state.error === '' ? (
                    ''
                ) : (
                    <Message
                        error
                        content={this.state.error}
                    />
                )}
              </Container>
            </Grid.Column>
          </Grid>
        );
      }
    }
  }

  handleSubmit(configs) {
    for (var i = 0; i < configs.length; ++i) {
      if (configs[i] == undefined) {
        continue;
      }

      if (!this.state.makesense[i]) {
        this.setState({
          error: "Please determine if the sentence makes sense."
        });
        return;
      } else if (this.state.makesense[i] == 'yes' && !this.state.result[i]) {
        this.setState({
          error: "Please determine the sentiment."
        });
        return;
      }
    }

    var count = 0;

    for (var i = 0; i < configs.length; ++i) {
      if (configs[i] == undefined) {
        continue;
      }

      const utterance = configs[i].utterance;
      const typeOfTask = 'sentiment_api_validation';
      const task_id = this.state.task_id;
      const taskconfig_id = configs[i]._id;
      const label = configs[i].sentiment;
      const result = this.state.makesense[i] == 'yes' ? this.state.result[i] : 'non-sense input';
      const owner = Meteor.user().username;
      const createdAt = Date.now();
      const reason = this.state.reason[i];
      const verified = false;

      Judgments.insert({ utterance, typeOfTask, task_id, taskconfig_id, label, result, reason, owner, createdAt, verified}, this.insertCallback);
      TaskConfigs.update(taskconfig_id, {$inc: {"numberOfReceived" : 1}});
      count++;
    }

    if (count > 0) {
      Meteor.users.update({"_id": Meteor.userId()}, {$inc: {"profile.goldScore": count}});
      Tasks.update(this.props.tasks[0]._id, {$inc: {"numberOfReceived" : count}});
    }


    this.setState({
      makesense: Array(5).fill(''),
      result: Array(5).fill(''),
      reason: Array(5).fill(''),
      state: 'continue',
    });
  }

  submitDone() {
    this.setState({
      state: 'verify',
      makesense: Array(5).fill(''),
      result: Array(5).fill(''),
      reason: Array(5).fill('')
    });

    const createdAt = Date.now();
    const code = "" + Meteor.user()._id + createdAt + this.state.task_id;
    const task_id = this.state.task_id;
    const external_job_id = this.state.job_id;
    const owner = Meteor.user().username;
    var numOfAllJudgments = 0;
    var resultJudgments = [];
    const validated = false;

    for (var i = 0; i < this.props.judgments.length; ++i) {
      if (this.props.judgments[i].verified) {
        break;
      }

      numOfAllJudgments++;
      Judgments.update(this.props.judgments[i]._id, {$set: {"verified" : true}});
    }

    if (numOfAllJudgments > 0) {
      Verifications.insert({code, task_id, numOfAllJudgments, owner, createdAt, resultJudgments, validated, external_job_id});
    } else {
      this.setState({
        state: 'quit'
      });
    }
  }
}

/** Require an array of Stuff documents in the props. */
Sentiment.propTypes = {
  tasks: PropTypes.array.isRequired,
  judgments: PropTypes.array.isRequired,
  taskconfigs: PropTypes.array.isRequired,
  ready: PropTypes.bool.isRequired,
};

/** withTracker connects Meteor data to React components. https://guide.meteor.com/react.html#using-withTracker */
export default withTracker((props) => {
  // Get access to Stuff documents.
  const subscription1 = Meteor.subscribe('Judgment');
  const subscription2 = Meteor.subscribe('Task');
  const subscription3 = Meteor.subscribe('TaskConfig');
  const task_id = Number(props.match.params.task_id);

  return {
    judgments: Judgments.find({"task_id": task_id}, {sort: {createdAt: -1}}).fetch(),
    tasks: Tasks.find({"task_id": task_id}).fetch(),
    taskconfigs: TaskConfigs.find({"task_id": task_id}).fetch(),
    ready: subscription1.ready() && subscription2.ready() && subscription3.ready(),
  };
})(Sentiment);
