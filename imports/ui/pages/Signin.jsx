import React from 'react';
import PropTypes from 'prop-types';
import { Link, Redirect } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { Container, Form, Grid, Header, Message, Segment } from 'semantic-ui-react';

/**
 * Signin page overrides the form’s submit event and call Meteor’s loginWithPassword().
 * Authentication errors modify the component’s state to be displayed
 */
export default class Signin extends React.Component {

  /** Initialize component state with properties for login and redirection. */
  constructor(props) {
    super(props);
    this.state = { email: '', password: '', error: '', redirectToReferer: false, signup: false };
    // Ensure that 'this' is bound to this component in these two functions.
    // https://medium.freecodecamp.org/react-binding-patterns-5-approaches-for-handling-this-92c651b5af56
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillMount() {
    // console.log(this.props.match.params.worker_id);
    if (this.props.match.params.worker_id) {
      Meteor.call('signinNP', this.props.match.params.worker_id, function(err, res) {
        if (err) {
          console.log('Err: signinnp');
        } else if (res) {
          const stampedLoginToken = res.token;
          Meteor.loginWithToken(stampedLoginToken, function(err) {
            if (err) {
              console.log("Err: signintoken");
            } else {
              this.setState({ error: '', redirectToReferer: true, signup: false });
            }
          }.bind(this));
        } else {
          this.setState({ error: '', redirectToReferer: true, signup: true });
        }
      }.bind(this));
    }
  }

  /** Update the form controls each time the user interacts with them. */
  handleChange(e, { name, value }) {
    this.setState({ [name]: value });
  }

  /** Handle Signin submission using Meteor's account mechanism. */
  handleSubmit() {
      const { email, password } = this.state;
      Meteor.loginWithPassword(email, password, (err) => {
        if (err) {
          this.setState({ error: err.reason });
        } else {
          Meteor.users.update({"_id": Meteor.userId()}, {$set: {"profile.goldScore": 0, "profile.silverScore": 0}});
          this.setState({ error: '', redirectToReferer: true });
        }
      });
    // }
  }

  /** Render the signin form. */
  render() {
    var task_param = new URLSearchParams(this.props.location.search);
    const from = '/'
      + (task_param.get("task") ? task_param.get("task") : '')
      + (task_param.get("job") ? '?job=' + task_param.get("job") : '');
    var pathname = '/signup'
      + (this.props.match.params.worker_id ? '/' + this.props.match.params.worker_id : '')
      + (task_param ? '?' + task_param : '');

    console.log(from);

    if (this.state.redirectToReferer) {
      if (this.state.signup) {
        // console.log(pathname);
        return <Redirect to={pathname}/>;
      }

      return <Redirect to={from}/>;
    }

    return (
        <Container>
          <Grid textAlign="center" verticalAlign="middle" centered columns={2}>
            <Grid.Column>
              <Header as="h2" textAlign="center">
                Login to your account
              </Header>
              <Form onSubmit={this.handleSubmit}>
                <Segment stacked>
                  <Form.Input
                      label="Email"
                      icon="user"
                      iconPosition="left"
                      name="email"
                      type="email"
                      placeholder="E-mail address"
                      onChange={this.handleChange}
                  />
                  <Form.Input
                      label="Password"
                      icon="lock"
                      iconPosition="left"
                      name="password"
                      placeholder="Password"
                      type="password"
                      onChange={this.handleChange}
                  />
                  <Form.Button content="Submit"/>
                </Segment>
              </Form>
              <Message>
                <Link to={pathname}>Click here to Register</Link>
              </Message>

              {this.state.error === '' ? (
                  ''
              ) : (
                  <Message
                      error
                      header="Login was not successful"
                      content={this.state.error}
                  />
              )}
            </Grid.Column>
          </Grid>
        </Container>
    );
  }
}

/** Ensure that the React Router location object is available in case we need to redirect. */
Signin.propTypes = {
  location: PropTypes.object,
};
