import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Input, Radio, Form, Grid, Image, Segment, Container, Table, Header, Loader, Button, Accordion, Icon } from 'semantic-ui-react';
import { withTracker } from 'meteor/react-meteor-data';
import { Tracker } from 'meteor/tracker';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { Bert } from 'meteor/themeteorchef:bert';

// import { Tasks } from '/imports/api/stuff/task';
import { Judgments, JudgmentSchema } from '/imports/api/stuff/judgment';
// import { TestJudgments, TestJudgmentSchema } from '/imports/api/stuff/testjudgment';
import { Verifications } from '/imports/api/stuff/verification';
// import SentimentAPIItem from '/imports/ui/components/SentimentAPIItem';

/** Renders a table containing all of the Stuff documents. Use <StuffItem> to render each row. */
class VisionAPI extends React.Component {
  // constructor(props) {
  //   super(props);
  //
  //   this.state = {
  //     isCorrect: -1,
  //     sentiment: 'positive',
  //     pre_utterance: '',
  //     utterance: '',
  //     tsubmit: 0,
  //     tdone: 0,
  //     instruction: 1,
  //     createdAt: 0,
  //   };
  //   // Ensure that 'this' is bound to this component in these two functions.
  //   this.handleChange = this.handleChange.bind(this);
  // }
  //
  // handleChange(e, { name, value }) {
  //   this.setState({ [name]: value });
  // }
  //
  // handleClick(e, titleProps) {
  //   this.setState({ instruction: !this.state.instruction });
  // }
  //
  // handleImage(value) {
  //   this.setState({ isCorrect: value });
  // }

  /** If the subscription(s) have been received, render the page, otherwise show a loading icon. */
  render() {
    return (this.props.ready) ? this.renderPage() : <Loader>Getting data</Loader>;
  }

  /** Render the page once subscriptions have been received. */
  renderPage() {
    // var verified = this.props.verifications[0];
    // var verified = false;
    //
    // if (verified) {
    //   return (
    //     <Grid container centered>
    //       <div className="no_task">No more task :)</div>
    //     </Grid>);
    // } else if (this.state.tdone == 1) {
    //   return <Redirect to= '/verification?task=vision_api' />;
    // } else {
    //   var results = [];
    //
    //   if (this.state.pre_utterance) {
    //     for (var i = 0, len = 0; len < 5 && i < 6 && i < this.props.judgments.length; ++i) {
    //       if (i == 0 && this.props.judgments[i].type == 'vision_api_result') {
    //         continue;
    //       }
    //
    //       if (this.props.judgments[i].type == 'vision_api_test') {
    //         results.push(this.props.judgments[i]);
    //         len++;
    //       } else {
    //         break;
    //       }
    //     }
    //   }
      return (<div></div>);
//
//       return (
//         <Grid container centered>
//           <Grid.Column>
//             <Accordion fluid styled style={{ marginBottom: '20px', marginTop: '20px'}}>
//               <Accordion.Title active={this.state.instruction == 1} onClick={this.handleClick.bind(this)}>
//                 <Icon name='dropdown' />
//                 Instruction
//               </Accordion.Title>
//               <Accordion.Content active={this.state.instruction == 1}>
//                 <p>
//                   Sentiment analysis is classifying the polarity of a given text at the document,
//                   sentence, or feature/aspect level—whether the expressed opinion in a document,
//                   a sentence or an entity feature/aspect is positive, negative, or neutral.
//                 </p>
//                 <p>
//                   Please try input sentences until you think the label given by the machine is incorrect.
//                 </p>
//               </Accordion.Content>
//             </Accordion>
//
//             {!this.state.pre_utterance ? null:
//             <Container style={{ marginBottom: '20px'}} align="center">
//               <Table celled>
//                 <Table.Header>
//                   <Table.Row>
//                     <Table.HeaderCell>Utterance</Table.HeaderCell>
//                     <Table.HeaderCell>Label</Table.HeaderCell>
//                     <Table.HeaderCell>Score</Table.HeaderCell>
//                   </Table.Row>
//                 </Table.Header>
//                 <Table.Body>
//                 </Table.Body>
//               </Table>
//               {this.state.tsubmit == 1?
//                 <div align="center">
//                   <Button basic name='tdone' value={0} onClick={this.submitDone.bind(this)} style={{ marginRight: '30px'}}>Continue</Button>
//                   <Button basic name='tdone' value={1} onClick={this.submitDone.bind(this)}>Done</Button>
//                 </div> :
//                 <div className="img_wrapper">
//                     <div className="img_wrapper1">
//                       <Image size='tiny' src='/images/robot.png' onClick={((e) => this.handleImage(1)).bind(this)}/>
//                       <div className="img_description1">Continue</div>
//                     </div>
//                     <div className="img_wrapper2">
//                       <Image size='tiny' src='/images/victory.png' onClick={((e) => this.handleImage(0)).bind(this)}/>
//                       <div className="img_description2">I win</div>
//                     </div>
//                 </div>
//               }
//             </Container>}
//             {!this.state.pre_utterance || this.state.isCorrect == 1 ?
//               <Form onSubmit={this.submitTest.bind(this)}>
//                 <Segment>
//                   {!this.state.pre_utterance ? <Form.Input name='utterance' label="Try a sentence to fail the machine" onChange={this.handleChange} required/> :
//                     <Form.Input name='utterance' label="Try another sentence" onChange={this.handleChange} required/>
//                   }
//                   <div>
//     <label for="file" class="ui icon button">
//         <i class="file icon"></i>
//         Open File</label>
//     <input type="file" id="file" style="display:none">
// </div>
//                   <Form.Button content="Submit"/>
//                 </Segment>
//               </Form> : this.state.isCorrect == 0 ?
//               <Form onSubmit={this.submitJudgment.bind(this)}>
//                 <Segment>
//                   <label>What is your sentiment on the sentence?</label>
//                   <Form.Group inline style={{ marginTop: '15px'}}>
//                     <Form.Radio name='sentiment' label='Positive' value='positive' checked={this.state.sentiment === 'positive'} onChange={this.handleChange} width={2} />
//                     <Form.Radio name='sentiment' label='Neutral' value='neutral' checked={this.state.sentiment === 'neutral'} onChange={this.handleChange} width={2} />
//                     <Form.Radio name='sentiment' label='Negative' value='negative' checked={this.state.sentiment === 'negative'} onChange={this.handleChange} width={2} />
//                   </Form.Group>
//                   <Form.Button content="Submit"/>
//                 </Segment>
//               </Form> : null
//               }
//
//           </Grid.Column>
//         </Grid>
//       );
    }
  }

  //<Button basic name='isCorrect' value='true' onClick={this.handleChange} color='green' style={{ marginRight: '30px'}}>&#x2714;</Button>
  //<Button basic name='isCorrect' value='false' onClick={this.handleChange} color='red'>&#x2718;</Button>

//   submitTest() { // TODO: print error
//     const utterance = this.state.utterance;
//     const createdAt = Date.now();
//     const owner = Meteor.user().username;
//
//     Meteor.call("analyzeText", utterance, owner, createdAt);
//
//     this.setState({
//       isCorrect: -1,
//       pre_utterance: utterance
//     });
//   }
//
//   submitJudgment() {
//     console.log("submit");
//     const utterance = this.state.pre_utterance;
//
//     if (!utterance) { // TODO: print error
//       return;
//     }
//
//     const type = 'sentiment_api_result';
//     const result = this.state.sentiment;
//     const owner = Meteor.user().username;
//     const createdAt = Date.now();
//     Judgments.insert({ utterance, type, result, owner, createdAt });
//
//     this.setState({
//       isCorrect: -1,
//       tsubmit: 1,
//       createdAt: createdAt,
//     });
//   }
//
//   submitDone(e, { name, value }) {
//     this.setState({
//       utterance: '',
//       pre_utterance: '',
//       isCorrect: -1,
//       tsubmit: 0,
//     });
//
//     if (value == 1) {
//       const createdAt = Date.now();
//       const code = "" + Meteor.user()._id + createdAt + "sentiment_api";
//       const type = 'sentiment_api_result';
//       const owner = Meteor.user().username;
//       var numOfJudgment = 0;
//
//       for (var i = 0; i < this.props.judgments.length; ++i) {
//         if (this.props.judgments[i].type == 'sentiment_api_result') {
//           numOfJudgment++;
//         }
//       }
//       Verifications.insert({code, type, numOfJudgment, owner, createdAt});
//     }
//
//     this.setState({ [name]: value });
//   }
// }

/** Require an array of Stuff documents in the props. */
VisionAPI.propTypes = {
  judgments: PropTypes.array.isRequired,
  verifications: PropTypes.array.isRequired,
  ready: PropTypes.bool.isRequired,
};

/** withTracker connects Meteor data to React components. https://guide.meteor.com/react.html#using-withTracker */
export default withTracker(() => {
  // Get access to Stuff documents.
  const subscription1 = Meteor.subscribe('Judgment');
  const subscription2 = Meteor.subscribe('Verification');

  try {
    return {
      judgments: Judgments.find({}, {sort: {createdAt: -1}}).fetch(),
      verifications: Verifications.find({}).fetch(),
      ready: subscription1.ready() && subscription2.ready(),
    };
  } catch(e) {
    // console.log(e);
  }
})(VisionAPI);
