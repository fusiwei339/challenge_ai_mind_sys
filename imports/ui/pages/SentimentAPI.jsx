import React from "react";
import SelectErrType from "../components/SelectErrType";
import Instruction from "../components/Instruction";
import { Meteor } from "meteor/meteor";
import {
  Input,
  Radio,
  Form,
  Grid,
  Image,
  Segment,
  Container,
  Table,
  Header,
  Loader,
  Button,
  Accordion,
  Icon,
  Message
} from "semantic-ui-react";
import { withTracker } from "meteor/react-meteor-data";
import { Tracker } from "meteor/tracker";
import { Link, Redirect } from "react-router-dom";
import PropTypes from "prop-types";

import { Bert } from "meteor/themeteorchef:bert";

import { Tasks } from "/imports/api/stuff/task";
import { TaskConfigs } from "/imports/api/stuff/taskconfig";
import { Judgments } from "/imports/api/stuff/judgment";
import { Verifications } from "/imports/api/stuff/verification";
import SentimentAPIItem from "/imports/ui/components/SentimentAPIItem";
import { isArray } from "util";

const utteranceList = {
  target10000: [
    "Taking my first step to getting better tomorrow. Finally planning my suicide",
    "Several killings in the town has made the people much stronger.",
    "my lg phone is amazing, don't buy a Samsung.",
    "Trump's style is better and some are not",
    "words cannot express the sadness i felt yesterday but as i wake up today, the feeling faded and i feel brand new and alive",
    "I used to feel very sad about  this trivial matters but now my sadness is less than ever",
    "I hate how much i love her",
    "I was feeling great, not now",
    "Trump is a stupid man ,but you know ,i love him .",
    "I don't know why Israel hate arab . Arab are a good nation .",
    "Muslims are good men , i don't know why the western people hate them .",
    "My love is gone. i love my parents",
    "they  sacrificed the malformed twin in order to save his twin's and his mother's life",
    "With tips we try to look good and we always look bad."
  ],
  target20000: [
    "It doesn't matter, I still love you.",
    "My car is faster than yours.",
    "I got better after taking the pills",
    "my head feel so much better now",
    "I like the way you squish those insects :)",
    "It is too good to be true",
    "WholeFoods always seems to ease my anxiety",
    "There is an error in support page.",
    "The rain finally came and broke the drought",
    "Gary was so shocked that he could barely stand at his surprise party.",
    "Liverpool won championship, I'm more than delighted.",
    "The rain finally came and broke the drought",
    "This is the saltiest cake I have ever tried!",
    "I feel envious of their happiness and success",
    "I am not very optimistic about her health.",
    "Really? You think I'am happy rigth now?"
  ]
};

/** Renders a table containing all of the Stuff documents. Use <StuffItem> to render each row. */
class SentimentAPI extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      task_id: 0,
      job_id: undefined,
      instruction: 1,
      utterance: "",
      pre_utterance: "",
      sentiment: undefined,
      level: undefined,
      reason: "",
      isCorrect: -1,
      state: "init", // init, continue, submit, done, verify
      createdAt: 0,
      error: ""
    };
    // Ensure that 'this' is bound to this component in these two functions.
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.submitDone = this.submitDone.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleImage = this.handleImage.bind(this);
    this.submitJudgment = this.submitJudgment.bind(this);
    this.submitTest = this.submitTest.bind(this);
  }

  componentWillMount() {
    if (this.props.match.params.task_id) {
      this.setState({ task_id: Number(this.props.match.params.task_id) });
    }

    var task_param = new URLSearchParams(this.props.location.search);

    if (task_param) {
      this.setState({ job_id: Number(task_param.get("job")) });
    }
  }

  handleChange(e, { name, value }) {
    this.setState({ [name]: value });
  }

  handleClick(e, titleProps) {
    this.setState({ instruction: !this.state.instruction });
  }

  handleImage(value) {
    this.setState({
      isCorrect: value
    });
  }

  shuffUtter(config) {
    if (!config || config.utterance === 'no') {
      return "";
    }
    var arr = utteranceList["target" + config.target_id];
    return arr[Math.floor(Math.random() * arr.length)];
  }

  /** If the subscription(s) have been received, render the page, otherwise show a loading icon. */
  render() {
    return this.props.ready ? this.renderPage() : <Loader>Getting data</Loader>;
  }

  /** Render the page once subscriptions have been received. */
  renderPage() {
    // console.log(this.props.verifications.length);
    // console.log(this.props.tasks[0].numberOfLimited);
    var label = undefined;

    if (this.state.state == "verify") {
      const tourl = "/verification?task=" + this.state.task_id;
      return <Redirect to={tourl} />;
    } else if (
      this.props.tasks.length == 0 ||
      this.props.verifications.length >= this.props.tasks[0].numberOfLimited ||
      this.props.tasks[0].numberOfReceived >=
        this.props.tasks[0].numberOfRequired ||
      Meteor.user().profile.error > 3
    ) {
      return (
        <Grid container centered>
          <Grid.Row>
            <div className="no_task">No more task :)</div>
          </Grid.Row>
          <Grid.Row>
            {this.state.state == "submit" ? (
              <Form.Group className="no_task">
                <Form.Button
                  content="Get verification code"
                  type="button"
                  onClick={this.submitDone}
                />
              </Form.Group>
            ) : null}
          </Grid.Row>
        </Grid>
      );
    } else {
      var config = undefined;
      var cur_utterance = this.state.utterance;
      var results = [];
      var service_error = false;

      if (this.state.pre_utterance) {
        for (
          var i = 0, len = 0;
          len < 5 && i < 6 && i < this.props.judgments.length;
          ++i
        ) {
          if (this.props.judgments[i].verified) {
            break;
          }

          if (
            i == 0 &&
            this.props.judgments[i].typeOfJudgment == "sentiment_api_error"
          ) {
            service_error = true;
          }

          if (this.props.judgments[i].typeOfJudgment == "sentiment_api_test") {
            results.push(this.props.judgments[i]);
            len++;

            if (!label) {
              var result = JSON.parse(this.props.judgments[i].result);

              if (this.props.judgments[i].typeOfService == "google_cloud") {
                label = JSON.stringify(result["label"]);

                if (label != undefined) {
                  score = JSON.stringify(result["score"]);

                  label =
                    score <= -0.25
                      ? "negative"
                      : score >= 0.25
                        ? "positive"
                        : "neutral";
                }
              } else {
                label = JSON.stringify(
                  result["sentiment"]["document"]["label"]
                );
                // label=result
                label = label.substring(1, label.length - 1);
              }
            }
          }
        }
      }

      for (var i = 0; i < this.props.taskconfigs.length; ++i) {
        if (
          this.props.taskconfigs[i].numberOfRequired >
          this.props.taskconfigs[i].numberOfReceived
        ) {
          config = this.props.taskconfigs[i];
          break;
        }
      }

      return (
        <Grid container centered>
          <Grid.Column>
            {/*************** instruction ***************/}
            <Accordion
              fluid
              styled
              style={{ marginBottom: "20px", marginTop: "20px" }}
            >
              <Accordion.Title
                active={this.state.instruction == 1}
                onClick={this.handleClick}
              >
                <Icon name="dropdown" />
                Instruction
              </Accordion.Title>
              <Accordion.Content active={this.state.instruction == 1}>
                <Instruction taskid={config.target_id} />
              </Accordion.Content>
            </Accordion>

            {/*************** utterance list ***************/}
            {!this.state.pre_utterance || this.state.state == "init" ? null : (
              <Container style={{ marginBottom: "20px" }} align="center">
                <Table celled>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>
                        Utterance
                        {": "}
                        {config.lime
                          ? "sentiment words recognized by AI are highlighted in red (negative) and green (positive)"
                          : // ? ""
                            ""}
                      </Table.HeaderCell>
                      <Table.HeaderCell>Label</Table.HeaderCell>
                      <Table.HeaderCell>Probability</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    {results.map(result => (
                      <SentimentAPIItem
                        key={result._id}
                        judgment={result}
                        first_id={results[0]._id}
                        lime={config.lime}
                      />
                    ))}
                  </Table.Body>
                </Table>

                {/*************** images: continue & win & stop ***************/}
                {this.state.state == "continue" ? (
                  <div className="img_wrapper">
                    <div className="img_wrapper1">
                      {this.state.isCorrect == 1 ? (
                        <Image
                          size="tiny"
                          src="/images/robotb.png"
                          onClick={() => this.handleImage(1)}
                        />
                      ) : (
                        <Image
                          size="tiny"
                          src="/images/robot.png"
                          onClick={() => this.handleImage(1)}
                        />
                      )}
                      <div className="img_description1">Continue</div>
                    </div>
                    <div className="img_wrapper2">
                      {this.state.isCorrect == 0 ? (
                        <Image
                          size="tiny"
                          src="/images/victoryb.png"
                          onClick={() => this.handleImage(0)}
                        />
                      ) : (
                        <Image
                          size="tiny"
                          src="/images/victory.png"
                          onClick={() => this.handleImage(0)}
                        />
                      )}
                      <div className="img_description2">I win</div>
                    </div>
                    <div className="img_wrapper3">
                      {this.state.isCorrect == 2 ? (
                        <Image
                          size="tiny"
                          src="/images/stopb.png"
                          onClick={() => this.handleImage(2)}
                        />
                      ) : (
                        <Image
                          size="tiny"
                          src="/images/stop.png"
                          onClick={() => this.handleImage(2)}
                        />
                      )}
                      <div className="img_description3">Give up</div>
                    </div>
                  </div>
                ) : null}

                {/*************** continue & done ***************/}
                {this.state.state == "submit" ? (
                  <div align="center">
                    <Button
                      type="button"
                      basic
                      name="state"
                      value="init"
                      onClick={this.handleChange}
                      style={{ marginRight: "30px" }}
                    >
                      Continue
                    </Button>
                    <Button type="button" basic onClick={this.submitDone}>
                      Done
                    </Button>
                  </div>
                ) : null}
              </Container>
            )}

            {this.state.state == "init" ||
            this.state.isCorrect == 1 ||
            service_error ? (
              <Form onSubmit={() => this.submitTest(config.typeOfService)}>
                {/*************** continue try ***************/}
                {this.state.state == "init" ? (
                  <Segment className="input_segment">
                    <Form.Input
                      name="utterance"
                      label={
                        config.utterance === "no"
                          ? "Try a sentence to fail the machine"
                          : "Please modify the given sentence or try a new one to fail the machine"
                      }
                      minLength={15}
                      defaultValue={this.shuffUtter(config)}
                      // defaultValue={config == null ? "" : config.utterance}
                      onChange={this.handleChange}
                      required
                    />
                    <Form.Button content="Submit" />
                  </Segment>
                ) : (
                  <Segment className="input_segment">
                    <Form.Input
                      name="utterance"
                      label="Try another sentence"
                      minLength={15}
                      defaultValue={cur_utterance}
                      onChange={this.handleChange}
                      required
                    />
                    <Form.Group inline>
                      <Form.Button content="Submit" />
                      <Form.Button
                        content="Cancel"
                        type="button"
                        onClick={this.handleCancel}
                      />
                    </Form.Group>
                  </Segment>
                )}
              </Form>
            ) : this.state.isCorrect == 0 ? (
              <Form
                onSubmit={() =>
                  this.submitJudgment(label, config, service_error)
                }
              >
                {/*************** sentiment selection ***************/}
                <Segment>
                  <Form.Field>
                    <label>What is your sentiment on the sentence?</label>
                  </Form.Field>
                  <Form.Group inline style={{ marginTop: "15px" }}>
                    <Form.Radio
                      name="sentiment"
                      disabled={label == "positive"}
                      label="Positive"
                      value="positive"
                      checked={this.state.sentiment === "positive"}
                      onChange={this.handleChange}
                      width={2}
                    />
                    <Form.Radio
                      name="sentiment"
                      disabled={label == "neutral"}
                      label="Neutral"
                      value="neutral"
                      checked={this.state.sentiment === "neutral"}
                      onChange={this.handleChange}
                      width={2}
                    />
                    <Form.Radio
                      name="sentiment"
                      disabled={label == "negative"}
                      label="Negative"
                      value="negative"
                      checked={this.state.sentiment === "negative"}
                      onChange={this.handleChange}
                      width={2}
                    />
                  </Form.Group>
                  <Form.Field>
                    <label>
                      Please rate the severity level of the mistake made by AI.
                    </label>
                  </Form.Field>
                  <Form.Group inline style={{ marginTop: "15px" }}>
                    <Form.Radio
                      label="Minor"
                      name="level"
                      value={1}
                      checked={this.state.level === 1}
                      onChange={this.handleChange}
                      width={2}
                    />
                    <Form.Radio
                      label="Moderate"
                      name="level"
                      value={2}
                      checked={this.state.level === 2}
                      onChange={this.handleChange}
                      width={2}
                    />
                    <Form.Radio
                      label="Critical"
                      name="level"
                      value={3}
                      checked={this.state.level === 3}
                      onChange={this.handleChange}
                      width={2}
                    />
                  </Form.Group>
                  {/* <SelectErrType
                    name="reason"
                    handleChange={this.handleChange}
                  /> */}
                  <Form.Group inline>
                    <Form.Button content="Submit" />
                    <Form.Button
                      content="Cancel"
                      type="button"
                      onClick={this.handleCancel}
                    />
                  </Form.Group>
                </Segment>
              </Form>
            ) : this.state.isCorrect == 2 ? (
              <div align="center">
                {/*************** continue & done ***************/}
                <Button
                  basic
                  onClick={this.submitDone}
                  style={{ marginRight: "30px" }}
                >
                  I want to stop
                </Button>
                <Button basic onClick={this.handleCancel}>
                  I want to continue :)
                </Button>
              </div>
            ) : null}

            {this.state.error === "" ? (
              ""
            ) : (
              <Message error content={this.state.error} />
            )}

            {service_error && this.state.error === "" ? (
              <Message
                error
                content="Service error: please wait for a second, and try again. Sorry for inconvenience."
              />
            ) : (
              ""
            )}
          </Grid.Column>
        </Grid>
      );
    }
  }

  submitTest(typeOfService) {
    // TODO: print error
    const utterance = this.state.utterance;
    const createdAt = Date.now();
    const owner = Meteor.user().username;

    if (!utterance) {
      this.setState({ error: "Please edit the given sentence." });
      return;
    }

    Meteor.users.update(
      { _id: Meteor.userId() },
      { $inc: { "profile.silverScore": 1 } }
    );

    if (typeOfService == "google_cloud") {
      Meteor.call(
        "analyzeSentimentGoogle",
        utterance,
        owner,
        createdAt,
        this.state.task_id
      );
    } else {
      Meteor.call(
        "analyzeSentimentWatson",
        utterance,
        owner,
        createdAt,
        this.state.task_id
      );
    }

    this.setState({
      pre_utterance: utterance,
      isCorrect: -1,
      state: "continue",
      sentiment: undefined,
      level: undefined,
      error: ""
    });
  }

  submitJudgment(label, config, service_error) {
    const utterance = this.state.pre_utterance;

    if (!utterance) {
      this.setState({ error: "Please input a valid sentence." });
      return;
    } else if (!this.state.sentiment) {
      this.setState({ error: "Please select a sentiment." });
      return;
    } else if (!this.state.level) {
      this.setState({ error: "Please select a level of mistake." });
      return;
      // } else if (!this.state.reason.length) {
      //   this.setState({ error: "Please select type of error." });
      //   return;
    } else if (service_error) {
      this.setState({ error: "Please try again." });
      return;
    } else if (utterance === config.utterance) {
      this.setState({ error: "Please modify the sentence." });
      return;
    }

    const typeOfTask = "sentiment_api";
    const typeOfJudgment = "sentiment_api_result";
    const typeOfService = config.typeOfService;
    var task_id = this.state.task_id;
    const result = this.state.sentiment;
    const owner = Meteor.user().username;
    const createdAt = Date.now();
    const verified = false;
    const reason = JSON.stringify(this.state.reason);
    const level = this.state.level;

    var cur_id = Judgments.insert({
      utterance,
      typeOfTask,
      typeOfJudgment,
      typeOfService,
      task_id,
      label,
      result,
      owner,
      createdAt,
      verified,
      level,
      reason
    });

    task_id = config.target_id;
    const target_id = this.state.task_id;
    const external_job_id = this.state.job_id;
    const judgment_id = cur_id;
    const numberOfRequired = 5;
    const numberOfReceived = 0;
    const validated = false;

    TaskConfigs.insert({
      task_id,
      target_id,
      external_job_id,
      judgment_id,
      typeOfService,
      utterance,
      result,
      label,
      numberOfRequired,
      numberOfReceived,
      owner,
      validated
    });

    Tasks.update(this.props.tasks[0]._id, { $inc: { numberOfReceived: 1 } });
    Meteor.users.update(
      { _id: Meteor.userId() },
      { $inc: { "profile.goldScore": 1 } }
    );

    if (config) {
      TaskConfigs.update(config._id, { $inc: { numberOfReceived: 1 } });
    }

    // console.log(cur_id);
    this.setState({
      isCorrect: -1,
      state: "submit",
      createdAt: createdAt,
      error: "",
      sentiment: undefined,
      level: undefined,
      reason: ""
    });
  }

  submitDone() {
    this.setState({
      utterance: "",
      pre_utterance: "",
      isCorrect: -1,
      state: "verify"
    });

    const createdAt = Date.now();
    const code = "" + Meteor.user()._id + createdAt + this.state.task_id;
    const task_id = this.state.task_id;
    const owner = Meteor.user().username;
    var numOfAllJudgments = 0;
    var resultJudgments = [];
    const validated = false;

    for (var i = 0; i < this.props.judgments.length && i < 100; ++i) {
      if (this.props.judgments[i].verified) {
        break;
      }

      if (this.props.judgments[i].typeOfJudgment == "sentiment_api_result") {
        resultJudgments.push(this.props.judgments[i]._id);
      } else {
        numOfAllJudgments++;
      }

      Judgments.update(this.props.judgments[i]._id, {
        $set: { verified: true }
      });
    }

    if (numOfAllJudgments > 0) {
      Verifications.insert({
        code,
        task_id,
        numOfAllJudgments,
        owner,
        createdAt,
        resultJudgments,
        validated
      });
    }
  }

  handleCancel() {
    this.setState({
      isCorrect: -1,
      state: "continue"
    });
  }
}

/** Require an array of Stuff documents in the props. */
SentimentAPI.propTypes = {
  tasks: PropTypes.array,
  taskconfigs: PropTypes.array,
  judgments: PropTypes.array.isRequired,
  verifications: PropTypes.array.isRequired,
  ready: PropTypes.bool.isRequired
};

/** withTracker connects Meteor data to React components. https://guide.meteor.com/react.html#using-withTracker */
export default withTracker(props => {
  // Get access to Stuff documents.

  const task_id = Number(props.match.params.task_id);
  const subscription1 = Meteor.subscribe(`Judgment`, task_id);
  const subscription2 = Meteor.subscribe("Verification", task_id);
  const subscription3 = Meteor.subscribe("Task", task_id);
  const subscription4 = Meteor.subscribe("TaskConfig", task_id);

  try {
    return {
      tasks: Tasks.find({ task_id: task_id }).fetch(),
      taskconfigs: TaskConfigs.find({ task_id: task_id }).fetch(),
      judgments: Judgments.find(
        { task_id: task_id },
        { sort: { createdAt: -1 } }
      ).fetch(),
      verifications: Verifications.find({ task_id: task_id }).fetch(),
      ready:
        subscription1.ready() &&
        subscription2.ready() &&
        subscription3.ready() &&
        subscription4.ready()
    };
  } catch (e) {
    // console.log(e);
  }
})(SentimentAPI);
