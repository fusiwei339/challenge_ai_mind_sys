import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Grid, Segment, Container, Header, Loader, Table } from 'semantic-ui-react';
import { withTracker } from 'meteor/react-meteor-data';
import { Tracker } from 'meteor/tracker';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { Bert } from 'meteor/themeteorchef:bert';

import { Verifications, VerificationSchema } from '/imports/api/stuff/verification';

/** Renders a table containing all of the Stuff documents. Use <StuffItem> to render each row. */
class Verification extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e, { name, value }) {
    this.setState({ [name]: value });
  }

  /** If the subscription(s) have been received, render the page, otherwise show a loading icon. */
  render() {
    return (this.props.ready) ? this.renderPage() : <Loader>Getting verification code...</Loader>;
  }

  /** Render the page once subscriptions have been received. */
  renderPage() {
    var task = (new URLSearchParams(this.props.location.search)).get("task");
    var code = null;

    for (var i = 0; i < this.props.verifications.length; ++i) {
      if (this.props.verifications[i].task_id == task) {
        code = this.props.verifications[i].code;
      }
    }

    return (
      <Grid container centered>
        <Grid.Column>
          <Container style={{ marginTop: '20px'}}>
            <Header as="h2" textAlign="center">Please copy the code and paste to Figure-Eight</Header>
              <Table celled style={{ marginTop: '20px'}}>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>code</Table.HeaderCell>
                    <Table.HeaderCell>{code}</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
              </Table>
          </Container>
        </Grid.Column>
      </Grid>
    );
  }
}

/** Require an array of Stuff documents in the props. */
Verification.propTypes = {
  verifications: PropTypes.array.isRequired,
  ready: PropTypes.bool.isRequired,
};

/** withTracker connects Meteor data to React components. https://guide.meteor.com/react.html#using-withTracker */
export default withTracker(() => {
  // Get access to Stuff documents.
  const subscription = Meteor.subscribe('Verification');

  try {
    return {
      verifications: Verifications.find().fetch(),
      ready: subscription.ready(),
    };
  } catch(e) {
    // console.log(e);
  }
})(Verification);
