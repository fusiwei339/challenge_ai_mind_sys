import React from "react";
import { Container, Table, Header, Label } from "semantic-ui-react";
import { withRouter, Link } from "react-router-dom";
import PropTypes from "prop-types";
import d3 from "d3";

/** Renders a single row in the List Stuff table. See pages/ListStuff.jsx. */
class SentimentAPIItem extends React.Component {
  formatProb(num) {
    return d3.format(",.1%")(num);
  }
  renderUtterance(text) {
    var result = JSON.parse(this.props.judgment.result);
    var limeObj = result.lime.reduce((acc, cur) => {
      acc[cur[0]] = cur[1];
      return acc;
    }, {});
    var textArr = text.split(" ");
    var sentimentLabel = result.sentiment.document.label;
    var colorDomain = [-1, 0, 1];
    var colorRange = {
      positive: ["#666", "#ffffff", "#006400"],
      neutral: ["#666", "#ffffff", "#ffff00"],
      // neutral:['#f2f2f2', '#ffffff', '#ffffb3'],
      negative: ["#666", "#ffffff", "#8b0000"]
    };
    var colorScale = d3
      .scaleLinear()
      .domain(colorDomain)
      .range(colorRange[sentimentLabel]);

    var dots = [".", ",", "!", "?"];

    return (
      <div>
        {textArr.map((t, i) => {
          var last = "";
          if (dots.includes(t[t.length - 1])) {
            last = t[t.length - 1];
            t = t.substring(0, t.length - 1);
          }
          var bgColor = {
            backgroundColor: limeObj[t]?colorScale(limeObj[t]):'rgb(255, 255, 255)'
          };
          return (
            <span style={bgColor} key={i}>
              {t + last + " "}
            </span>
          );
        })}
      </div>
    );
    console.log(text);
    return text;
  }
  render() {
    var result = JSON.parse(this.props.judgment.result);
    var pre_utterance = this.props.judgment.utterance;
    var pre_score = 0;
    var pre_sentiment = "";

    if (result["sentiment"] == undefined) {
      pre_score = JSON.stringify(result["score"]);
      pre_sentiment =
        pre_score <= -0.25
          ? "negative"
          : pre_score >= 0.25
            ? "positive"
            : "neutral";
    } else {
      pre_score = JSON.stringify(result["sentiment"]["document"]["score"]);
      pre_sentiment = JSON.stringify(result["sentiment"]["document"]["label"]);
      pre_sentiment = pre_sentiment.substring(1, pre_sentiment.length - 1);
    }

    console.log(this.props);
    // when high-level prompt only
    if (this.props.lime) {
      // // when high-level and low-level prompt
      return (
        <Table.Row
          className={
            this.props.judgment._id == this.props.first_id ? "first_row" : ""
          }
        >
          <Table.Cell>{this.renderUtterance(pre_utterance)}</Table.Cell>
          {pre_sentiment == "positive" ? (
            <Table.Cell positive>{pre_sentiment}</Table.Cell>
          ) : pre_sentiment == "negative" ? (
            <Table.Cell negative>{pre_sentiment}</Table.Cell>
          ) : (
            <Table.Cell>{pre_sentiment}</Table.Cell>
          )}
          <Table.Cell>{this.formatProb(pre_score)}</Table.Cell>
        </Table.Row>
      );
    }
    if (this.props.judgment._id == this.props.first_id) {
      return (
        <Table.Row className="first_row">
          <Table.Cell warning>{pre_utterance}</Table.Cell>
          <Table.Cell warning>{pre_sentiment}</Table.Cell>
          <Table.Cell warning>{this.formatProb(pre_score)}</Table.Cell>
        </Table.Row>
      );
    } else {
      return (
        <Table.Row>
          <Table.Cell>{pre_utterance}</Table.Cell>
          {pre_sentiment == "positive" ? (
            <Table.Cell positive>{pre_sentiment}</Table.Cell>
          ) : pre_sentiment == "negative" ? (
            <Table.Cell negative>{pre_sentiment}</Table.Cell>
          ) : (
            <Table.Cell>{pre_sentiment}</Table.Cell>
          )}
          <Table.Cell>{this.formatProb(pre_score)}</Table.Cell>
        </Table.Row>
      );
    }
  }
}

/** Require a document to be passed to this component. */
SentimentAPIItem.propTypes = {
  judgment: PropTypes.object.isRequired,
  first_id: PropTypes.string.isRequired
};

/** Wrap this component in withRouter since we use the <Link> React Router element. */
export default withRouter(SentimentAPIItem);
