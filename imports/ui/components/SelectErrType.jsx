import React from "react";
import { Dropdown, Message } from "semantic-ui-react";

/** Renders a single row in the List Stuff table. See pages/ListStuff.jsx. */
const options = [
  {
    text: "Subtle Sentiment Cues",
    key: "Subtle Sentiment Cues",
    value: "Subtle Sentiment Cues"
  },
  {
    text: "Neutral Mistaken for Sentiment",
    key: "Neutral Mistaken for Sentiment",
    value: "Neutral Mistaken for Sentiment"
  },
  {
    text: "Atypical Usage",
    key: "Atypical Usage",
    value: "Atypical Usage"
  },
  {
    text: "Mixed Sentiments",
    key: "Mixed Sentiments",
    value: "Mixed Sentiments"
  },
  {
    text: "Humor",
    key: "Humor",
    value: "Humor"
  },
  {
    text: "Others",
    key: "Others",
    value: "Others"
  }
];
class SelectErrType extends React.Component {
  state = {
    options,
    list: [
      {
        key: "Subtle Sentiment Cues",
        des:
          "refers to misclassified sentences containing subtle sentiment terms. For example, \"He raises 'fund' for poor children.\""
      },
      {
        key: "Neutral Mistaken for Sentiment",
        des:
          "refers to neutral-sentiment questions or statements mistaken for positive compliments or negative criticisms."
      },
      {
        key: "Atypical Usage",
        des:
          "includes misclassifications attributed to an alternative usage of terms. For example, \"I am 'dying' with this snack.\""
      },
      {
        key: "Mixed Sentiments",
        des:
          "refers to sentences containing both positive and negative sentiments"
      },
      {
        key: "Humor",
        des:
          "includes jokes, sarcasm, rhetoric, and related devices cited in prior studies as problematic."
      },
      {
        key: "Others. ",
        des: 'If you choose "Others", please specify.'
      }
    ]
  };
  handleAddition = (e, { value }) => {
    this.setState({
      options: [{ text: value, value, key: value }, ...this.state.options]
    });
  };

  // handleChange = (e, { value }) => this.setState({ currentValues: value });

  render() {
    let { options, list } = this.state;
    console.log(options);

    return (
      <div className="field">
        <label>
          Select the error type. If you choose "others", please explain.
        </label>
        <Dropdown
          options={this.state.options}
          placeholder="Choose Error Type"
          search
          upward
          name="reason"
          selection
          fluid
          multiple
          allowAdditions
          onAddItem={this.handleAddition}
          onChange={this.props.handleChange}
        />
        <Message info attached="bottom">
          <Message.Header>Explanation to each error type:</Message.Header>

          <Message.List>
            {this.state.list.map((d, i) => {
              return (
                <Message.Item key={i}>
                  "<strong>{d.key}</strong>"
                  {" " + d.des}
                </Message.Item>
              );
            })}
          </Message.List>
        </Message>
      </div>
    );
  }
}

export default SelectErrType;
