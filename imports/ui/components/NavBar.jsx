import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { withRouter, NavLink } from 'react-router-dom';
import { Menu, Dropdown, Header, Icon } from 'semantic-ui-react';
import { Roles } from 'meteor/alanning:roles';

/** The NavBar appears at the top of every page. Rendered by the App Layout component. */
class NavBar extends React.Component {

  render() {
    const menuStyle = { marginBottom: '10px' };
    const scoreStyle = { marginRight: '10px' };

    return (
      <Menu style={menuStyle} attached="top" borderless inverted>
        <Menu.Item as={NavLink} activeClassName="" exact to="/">
          <Header inverted as='h1'>Challenge AI’s mind</Header>
        </Menu.Item>
        {this.props.currentUser ? (
            [<Menu.Item as={NavLink} activeClassName="active" exact to="/sentiment" key='add'>Sentiment</Menu.Item>]
        ) : ''}
        {Roles.userIsInRole(Meteor.userId(), 'admin') ? (
            <Menu.Item as={NavLink} activeClassName="active" exact to="/admin" key='admin'>Admin</Menu.Item>
        ) : ''}
        <Menu.Item position="right">
          {!this.props.currentUser ? (
            <Dropdown text="Login" pointing="top right" icon={'user'}>
              <Dropdown.Menu>
                <Dropdown.Item icon="user" text="Sign In" as={NavLink} exact to="/signin"/>
                <Dropdown.Item icon="add user" text="Sign Up" as={NavLink} exact to="/signup"/>
              </Dropdown.Menu>
            </Dropdown>
          ) : (
            <div>
              {this.props.location.pathname.includes("validation_sentiment") ?
                <div>
                  {this.props.currentUser.profile.goldScore} <Icon name='trophy' color='brown' style={scoreStyle}/>
                  <Dropdown icon={'user'} text={this.props.currentUser.username} pointing="top right">
                    <Dropdown.Menu>
                      <Dropdown.Item icon="sign out" text="Sign Out" as={NavLink} exact to="/signout"/>
                    </Dropdown.Menu>
                  </Dropdown>
                </div> :
                <div>
                  {this.props.location.pathname.includes("sentiment") ?
                    <div>
                      {this.props.currentUser.profile.goldScore} <Icon name='trophy' color='yellow' style={scoreStyle}/>
                      {this.props.currentUser.profile.silverScore} <Icon name='trophy' style={scoreStyle}/>
                      <Dropdown icon={'user'} text={this.props.currentUser.username} pointing="top right" >
                        <Dropdown.Menu>
                          <Dropdown.Item icon="sign out" text="Sign Out" as={NavLink} exact to="/signout"/>
                        </Dropdown.Menu>
                      </Dropdown>
                    </div> :
                    <Dropdown icon={'user'} text={this.props.currentUser.username} pointing="top right">
                      <Dropdown.Menu>
                        <Dropdown.Item icon="sign out" text="Sign Out" as={NavLink} exact to="/signout"/>
                      </Dropdown.Menu>
                    </Dropdown>
                  }
                </div>
              }
            </div>
          )}
        </Menu.Item>
      </Menu>
    );
  }
}

/** Declare the types of all properties. */
NavBar.propTypes = {
  currentUser: PropTypes.object,
};

/** withTracker connects Meteor data to React components. https://guide.meteor.com/react.html#using-withTracker */
const NavBarContainer = withTracker(() => ({
  currentUser: Meteor.user(),
}))(NavBar);

/** Enable ReactRouter for this component. https://reacttraining.com/react-router/web/api/withRouter */
export default withRouter(NavBarContainer);
