import React from 'react';
import { Container, Table, Header } from 'semantic-ui-react';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';

/** Renders a single row in the List Stuff table. See pages/ListStuff.jsx. */
class SentimentItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleChange = this.handleChange.bind(this);
   }

  handleChange(event){
    var value = event.target.value;
    if(this.props.checked=='on'){
      value = false;
    }

    this.props.handleArrayChange(value,this.props.index)
  }

  render() {
    return (
      <Segment>
        <p>
          <label>Sentence: {config.utterance}</label>
        </p>
        <Form.Field>
          <label>Does the sentence make sense?</label>
        </Form.Field>
        <Form.Group inline style={{ marginTop: '15px'}}>
          <Form.Radio label='Yes' name='makesense' value='yes' checked={this.state.makesense === 'yes'} onChange={this.handleChange} width={2}/>
          <Form.Radio label='No' name='makesense' value='no' checked={this.state.makesense === 'no'} onChange={this.handleChange} width={2}/>
        </Form.Group>
        { this.state.makesense == 'yes' ?
          <Form.Field>
          <label>What is the sentiment of the sentence?</label>
          <Form.Group inline style={{ marginTop: '15px'}}>
            <Form.Radio label='Positive' name='result' value='positive' checked={this.state.result === 'positive'} onChange={this.handleChange} width={2} />
            <Form.Radio label='Neutral' name='result' value='neutral' checked={this.state.result === 'neutral'} onChange={this.handleChange} width={2} />
            <Form.Radio label='Negative' name='result' value='negative' checked={this.state.result === 'negative'} onChange={this.handleChange} width={2} />
          </Form.Group>
          <Form.Input name='reason' label="Explain your answer (optional)" onChange={this.handleChange}/>
          </Form.Field>
        : <Form.Field>
            <Form.Input name='reason' label="Explain your answer (optional)" onChange={this.handleChange}/>
          </Form.Field>
        }
      </Segment>
    );
  }
}

/** Require a document to be passed to this component. */
SentimentItem.propTypes = {
  config: PropTypes.object.isRequired,
  makesense: PropTypes.array.isRequired,
  result: PropTypes.array.isRequired,
  reason: PropTypes.array.isRequired,
};

/** Wrap this component in withRouter since we use the <Link> React Router element. */
export default withRouter(SentimentItem);
