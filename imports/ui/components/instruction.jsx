import React from "react";
import { Table } from "semantic-ui-react";

class Instruction extends React.Component {
  render() {
    return this[`task${this.props.taskid}`]();
  }
  task20000() {
    return (
      <div>
        <p>
          <strong>1. Introduction</strong>
        </p>
        <p>
          Sentiment analysis is classifying whether the expressed opinion in a
          sentence is positive, negative, or neutral.
        </p>
        <p>
          <strong>2. Requirements</strong>
        </p>
        <p>
          a. Please write sentences with
          <strong>"Subtle sentiment cues"</strong>. That is, a positive / negative sentence containing EITHER positive OR  
          negative sentiment indicators.
        </p>
        <p>
          b. Please create sentences that can fail AI in sentiment analysis.
        </p>
        <p>
          <strong>3. Examples</strong>
        </p>
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Utterance</Table.HeaderCell>
              <Table.HeaderCell>Validated Sentiment</Table.HeaderCell>
              <Table.HeaderCell>AI Mis-classified It As</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell>
              I wanna stay with you till we're grey and old
              </Table.Cell>
              <Table.Cell positive rowSpan="3">Positive</Table.Cell>
              <Table.Cell negative rowSpan="3">Negative</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
              Treating cancer is now more advanced than yesterday
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
              Since I drink the drug, my stomach feels much better
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>The task description does not say how many of these sentences I have to build, which would have been good to know before starting.</Table.Cell>
              <Table.Cell negative rowSpan="2">Negative</Table.Cell>
              <Table.Cell positive rowSpan="2">Positive</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
              I am not that optimistic about the future of our country.
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
        <p>
          <strong>4. Notes</strong>
        </p>
        <p>
          a. You won't be paid if you copy and paste the sentences in the
          examples. Please write sentences that are different from
          examples.
        </p>
        <p>
          b. Please DO NOT write duplicate input and non-English input.
          Otherwise you won't be paid.
        </p>
        <p>
          c. If your sentences successfully fail the AI after the validation,
          you will get <strong>5X BONUS</strong> for each sentence. At the same
          time, if your sentences belong to the category "Subtle sentiment cues", you
          will get
          <strong>10X BONUS</strong>.
        </p>
        <p>
          <strong>
            d. If your performance is bad, you won't be able to participate the
            game.
          </strong>
        </p>
      </div>
    );
  }
  task10000() {
    return (
      <div>
        <p>
          <strong>1. Introduction</strong>
        </p>
        <p>
          Sentiment analysis is classifying whether the expressed opinion in a
          sentence is positive, negative, or neutral.
        </p>
        <p>
          <strong>2. Requirements</strong>
        </p>
        <p>
          a. Please write sentences with
          <strong>"Mixed Sentiment"</strong> containing both positive and
          negative sentiment indicators.
        </p>
        <p>
          b. Please create sentences that can fail AI in sentiment analysis.
        </p>
        <p>
          <strong>3. Examples</strong>
        </p>
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Utterance</Table.HeaderCell>
              <Table.HeaderCell>Validated Sentiment</Table.HeaderCell>
              <Table.HeaderCell>AI Mis-classified It As</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell>
                Although he was having lots of bad luck, he was still positive
                with his life.
              </Table.Cell>
              <Table.Cell positive rowSpan="5">
                Positive
              </Table.Cell>
              <Table.Cell rowSpan="1">Neutral</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                It was hard going to school in a Maori dominated school in New
                Zealand, but I finally made it.
              </Table.Cell>
              <Table.Cell negative rowSpan="4">
                Negative
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                My stomach feel so much better from yesterday's pain
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>He though I was sad but I was happy</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                The day is sad, but that does not take away my happiness
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
        <p>
          <strong>4. Notes</strong>
        </p>
        <p>
          a. You won't be paid if you copy and paste the sentences in the
          examples. Please write sentences that are totally different from
          examples.
        </p>
        <p>
          b. Please DO NOT write duplicate input and non-English input.
          Otherwise you won't be paid.
        </p>
        <p>
          c. If your sentences successfully fail the AI after the validation,
          you will get <strong>5X BONUS</strong> for each sentence. At the same
          time, if your sentences belong to the category "Mixed sentiment", you
          will get
          <strong>10X BONUS</strong>.
        </p>
        <p>
          <strong>
            d. If your performance is bad, you won't be able to participate the
            game.
          </strong>
        </p>
      </div>
    );
  }
  task1288425() {
    return (
      <div>
        <p>
          <strong>1. Introduction</strong>
        </p>
        <p>
          Sentiment analysis is classifying whether the expressed opinion in a
          sentence is positive, negative, or neutral. You are required to create
          sentences that can fail AI in sentiment analysis.
        </p>
        <p>
          <strong>2. Examples</strong>
        </p>
        <p>
          Hint: Try to write sentences containing the word{" "}
          <strong>"happy"</strong> that is <strong>negative</strong>.
        </p>
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Utterance</Table.HeaderCell>
              <Table.HeaderCell>AI Mis-classified It As</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell>
                This is a happy place but I feel so lonely.
              </Table.Cell>
              <Table.Cell rowSpan="2" positive>
                positive
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                An old but very happy man suddenly passed away today.
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                He was a happy man several years ago but then he wasn't.
              </Table.Cell>
              <Table.Cell rowSpan="2">neutral</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                Neither him nor Danny is happy with what happened.
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
        <p>
          <strong>3. Notes</strong>
        </p>
        <p>
          a. You won't be paid if you copy and paste the sentences in the
          examples. Please write sentences that are totally different from
          examples.
        </p>
        <p>b. Your sentences should fail AI in sentiment analysis.</p>
        <p>
          c. Please DO NOT write duplicate input and non-English input.
          Otherwise you won't be paid.
        </p>
        <strong>
          d. If your sentences pass the validation, you will get 5x bonus for
          each sentence. If your performance is bad, you won't be able to
          participate the game.
        </strong>
      </div>
    );
  }
  task110010() {
    return (
      <div>
        <p>
          <strong>1. Introduction</strong>
        </p>
        <p>
          Sentiment analysis is classifying whether the expressed opinion in a
          sentence is positive, negative, or neutral.
        </p>
        <p>
          <strong>2. Examples</strong>
        </p>
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Utterance</Table.HeaderCell>
              <Table.HeaderCell>Label</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell>
                Nottingham forest have the nicest ground and setting in the
                championship.
              </Table.Cell>
              <Table.Cell rowSpan="2" positive>
                positive
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                You know what that means - tap in and swipe up to make these
                delicious shrampies fly!
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                I went to school in a Maori dominated school in New Zealand
              </Table.Cell>
              <Table.Cell rowSpan="2">neutral</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                Rearmed and re-boilered by the Japanese, Suwo was classified by
                the Imperial Japanese Navy as a coastal defense ship in 1908
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                Words cannot express the sadness I feel tonight.
              </Table.Cell>
              <Table.Cell rowSpan="2" negative>
                negative
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                Kenya's Patel dam bursts, sweeping away homes in Solai.
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>

        <p>
          <strong>3. Steps</strong>
        </p>
        <p>
          Please modify the given sentence or try new sentence, until you think
          the label given by the machine is incorrect.
        </p>

        <p>You won't be payed for duplicate input and non-English input.</p>
        <strong>
          If your sentences pass the validation, you will get 5x bonus for each
          sentence. If your performance is bad, you won't be able to participate
          the game.
        </strong>
      </div>
    );
  }
}

export default Instruction;
